import picos as pcs
import cvxopt as cvx
import os
from sympy.functions.special.tensor_functions import KroneckerDelta
import numpy as np
import mosek
import matplotlib.pyplot as plt

os.system('clear')
print("================== Upper bound (Dual formulation) ==================")

def entsolv(alpha,efficiency,addnoise):
    olap = np.exp(-np.sqrt(efficiency)*alpha)
    px = .5
    print("\nOverlap value : ",olap)

    dim = 2
    i2 = cvx.matrix([1.,0.,0.,1.],(2,2)) #identity matrix
    if (addnoise == True):
        err=0.001
        pbx = cvx.matrix([olap,1-olap-err,err,olap,err,1-olap-err],(3,2))
    else:
        pbx = cvx.matrix([olap,1-olap,0,olap,0,1-olap],(3,2))


    #Basis & state defintion
    b0 = phi0 = cvx.matrix([1.,0.],(2,1))
    b1 = cvx.matrix([0.,1.],(2,1))
    phi1 = olap*b0 + np.sqrt(1-np.square(olap))*b1
    rho = [ phi0*phi0.T, phi1*phi1.T ]

    #SDP dual definition
    boundD=pcs.Problem()

    rho = pcs.new_param('rho',rho)
    pbx = pcs.new_param('pbx',pbx)
    i2 = pcs.new_param('i2',i2)

    nu = [ boundD.add_variable('nu{0}'.format(i),1) for i in range(6) ]
    H = [ boundD.add_variable('H[{0}]'.format(i),(2,2),vtype='hermitian') for i in range(4) ]

    for i in range(3):
        boundD.add_list_of_constraints(
                [rho[0]*(.5*float(KroneckerDelta(i,2))*(j%2)+.5*np.abs(j % 2-1)*(1-float(KroneckerDelta(i,2)))+nu[i])+rho[1]*(.5*float(KroneckerDelta(i,2))*float(KroneckerDelta(max(j-1,0),0))+.5*(1-float(KroneckerDelta(i,2)))*float(KroneckerDelta(min(j-2,0),0)) +nu[i+3])+H[j]-.5*('I'|H[j])*i2 << 0 for j in range(4)],
                'j'
                )

    Objsum=0
    for i in range(6):
        Objsum += nu[i]*pbx[i]

    boundD.set_objective('min',-Objsum)
    #boundD.solver_via_dual = True
    boundD.solve(solver='mosek',mosek_params={'presolve_tol_x' : 1.0e-15}, verbose=0)
    Objsum=0
    for i in range(6):
        Objsum += nu[i]*pbx[i]
    Entropy = - np.log2(-Objsum.value)
    print(boundD.status)
    print(alpha,Entropy)
    return Entropy

#Main
print("Alpha (mean number of photon) limit :")
limin = input("Min alpha :") or 0
limin = float(limin)
limax = input("Max alpha :") or 2
limax = float(limax)
step = input("Step :") or .2
step = float(step)
efficiency = input("Efficiency :") or 1.
efficiency = float(efficiency)
addnoise = input("Add noise : ") or True
tmp = limax/step
evol = np.zeros(int(tmp)+1)
x = np.arange(int(tmp)+1)
x = x*step
for i in range(int(limin)+1,int(tmp)+1):
    alpha = step*i
    evol[i]=entsolv(alpha,efficiency,addnoise)

print(evol)
ent = plt.plot(x,evol)
plt.xlabel('Mean number of photon')
plt.ylabel('H min')
plt.title('Evolution of min entropy with mean number of photon')
plt.setp(ent, color='r', linewidth=2.0)
plt.axis([limin,limax,min(evol),max(evol)])
plt.show()
