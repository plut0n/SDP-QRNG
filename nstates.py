import picos as pcs
import cvxopt as cvx
import os
import time as time
import sys
from cursesmenu import SelectionMenu
from sympy.functions.special.tensor_functions import KroneckerDelta
import numpy as np
import mosek
import matplotlib.pyplot as plt

os.system('clear')
print("================== Upper bound, n inputs case, pulse encoding (Dual formulation) ====================")

def constr(i,j,n,rho,px,nu): #First term (sum) of the SDP constraint w/ indexation /!\ Artisticly optimal, do not touch!
    cond=rho[0]*(px*float(KroneckerDelta(max(j,np.power(2,n-1)-1)-(np.power(2,n-1)-1),0))*float(KroneckerDelta(i,n)) + px*float(KroneckerDelta(min(j,np.power(2,n-1))-np.power(2,n-1),0))*(1-float(KroneckerDelta(i,n))) + nu[i])
    for k in range(1,n-1):
        cond+=rho[k]*(px*float(KroneckerDelta(max(j%(np.power(2,n-k)),(np.power(2,n-(k+1))-1)-(np.power(2,n-(k+1))-1)),0))*float(KroneckerDelta(i,n)) + px*float(KroneckerDelta(min(j%(np.power(2,n-k)),(np.power(2,n-(k+1))))-np.power(2,n-k-1),0))*(1-float(KroneckerDelta(i,n))) + nu[i+(n+1)*k])
    cond+=rho[n-1]*(px*np.abs((j%2)-1)*float(KroneckerDelta(i,n)) + px*(j%2)*(1-float(KroneckerDelta(i,n))) + nu[i+(n+1)*(n-1)])
    return cond

def entsolvDn(n,alpha,efficiency,addnoise):
    olap = np.exp(-efficiency*alpha) #overlap(efficiency,mean photon number)
    olap_s = np.exp(-alpha)
    px = 1./float(n) #balanced input

    dim = n #n-D Hilbert Space
    identity = cvx.matrix([float(KroneckerDelta(i%(n+1),0)) for i in range(n*n)],(n,n)) #identity matrix
    if (addnoise == True):
        err=0.001
        pbx=cvx.matrix([err for i in range((n+1)*n)],(n+1,n))
        for i in range(n):
            pbx[n+i*(n+1)]=olap
            pbx[i*(n+2)]=1-olap-n*err
    else:
        pbx = cvx.matrix([1-olap,0,0,olap,
            0,1-olap,0,olap,
            0,0,1-olap,olap],(4,3))

    #Basis & state defintion
    basis = dict()
    for i in range(n):
        basis[i]=cvx.matrix([float(KroneckerDelta(j,i)) for j in range(n)],(n,1))

    #Declaring and building n input states w/ const overlap
    phi = dict()
    for i in range(n):
        phi[i]=[0. for i in range(n)]
    #constructing manually two first states
    phi[0][0]=1
    phi[0]=cvx.matrix(phi[0],(n,1))
    phi[0]=cvx.matrix(phi[0],tc='z')
    phi[1][0]=olap_s
    phi[1][1]=np.sqrt(1-np.square(olap_s))
    phi[1]=cvx.matrix(phi[1],(n,1))
    phi[1]=cvx.matrix(phi[1],tc='z')
    #Constructing every other states
    for i in range(2,n):
        tmp=0
        for j in range(i-1):
            phi[i][j]=phi[i-1][j] #every factor of the n-2 basis (w/ n number of states) is similar to the n-1's state
            tmp+=np.square(phi[i][j])
        #Const overlap
        phi[i][i-1]=(olap_s-tmp)/(phi[i-1][i-1]) #Elegant isn't it?
        #Normalization
        tmp=0
        for j in range(i):
            tmp+=np.square(phi[i][j])
        phi[i][i]=np.sqrt(1-tmp)
        #into cvx.matrix form
        phi[i]=cvx.matrix(phi[i],(n,1))
        phi[i]=cvx.matrix(phi[i],tc='z')

    #Density matrix
    rho = [ phi[i]*phi[i].H for i in range(n)]

    #SDP dual definition
    boundD=pcs.Problem()

    #Parameter
    rho = pcs.new_param('rho',rho)
    pbx = pcs.new_param('pbx',pbx)
    idendity = pcs.new_param('identity',identity)
    px = pcs.new_param('px',px)

    #Variable definition
    nu = [ boundD.add_variable('nu{0}'.format(i),1) for i in range(n*(n+1)) ]
    H = [ boundD.add_variable('H[{0}]'.format(i),(n,n),vtype='hermitian') for i in range(np.power(2,n)) ]

    #Such that:
    for i in range(n+1): #on every ouput
        boundD.add_list_of_constraints(
                [ constr(i,j,n,rho,px,nu)+H[j]-px*('I'|H[j])*idendity << 0 for j in range(np.power(2,n))],
                'j'
                )

    Objsum=0
    for i in range(n*(n+1)):
        Objsum += nu[i]*pbx[i]
    #Minimize : (objective)
    boundD.set_objective('min',-Objsum)
    #print(boundD)
    boundD.solve(solver='mosek',mosek_params={'presolve_tol_x' : 1.0e-15}, verbose=0)
    Objsum=0
    for i in range(n*(n+1)):
        Objsum += nu[i]*pbx[i]
    Entropy = - np.log2(-Objsum.value)
    #print(boundD.status)
    #print(alpha,Entropy)
    return Entropy

def plotting(ninstart,nin,evol,xlab,ylab,title,minax,maxax):
    ent = dict()
    for i in range(ninstart,nin+1):
        ent[i]=plt.plot(x,evol[i],label='%s inputs' %i)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.title(title)
    plt.axis([minax,maxax,0.0,max(evol[nin]+max(evol[nin])*.1)])
    plt.legend(loc='upper right')
    plt.show()


#Main
Selection = ["N inputs selection, plot Hmin(mean photon number)","N to M inputs, plot Hmin(mean)","Efficiency Modulation for 2 to N inputs, plot max(Hmin)(efficiency)"]
choice = SelectionMenu.get_selection(Selection,"N inputs, SDP dual formulation","Mod selection: ")

if(choice == 3):
    print("Exiting...")
    quit()

print("Alpha (mean number of photon) limit :")
limin = float(input("Min alpha^2: ") or 0.)
limax = float(input("Max alpha^2: ") or 2.5)
step = float(input("Step between two alpha^2: ") or .05)
tmp = limax/step
addnoise = input("Add noise: ") or True

if(choice == 0):
    nin = int(input("Number of inputs: ") or 2)
    ninstart = nin
    efficiency = float(input("Efficiency: ") or .77)
    evol = dict()
    evol[nin] = np.zeros(int(tmp)+1) #mh.. I know...
    x=np.arange(int(tmp)+1)
    x=x*step
    for i in range(int(limin)+1,int(tmp)+1):
        alpha = step*i
        evol[nin][i] = entsolvDn(nin,alpha,efficiency,addnoise)
    xlab='Mean number of photon (alpha^2)'
    ylab='H_min'
    title='Min Entropy evolution w/ Mean number of photon (efficiency = '+str(efficiency)+')'
    plotting(ninstart,nin,evol,xlab,ylab,title,limin,limax)

if(choice == 1):
    ninstart = int(input("Min number of inputs: ") or 2)
    nin = int(input("Max number of inputs: ") or 3)
    efficiency = float(input("Efficiency: ") or .77)
    evol=dict()
    for i in range(ninstart, nin+1):
        evol[i]=np.zeros(int(tmp)+1)
    x=np.arange(int(tmp)+1)
    x=x*step
    for i in range(ninstart,nin+1):
        print("\nNb of inputs: ",i)
        for j in range(int(limin)+1,int(tmp)+1):
            alpha = step*j
            evol[i][j]=entsolvDn(i,alpha,efficiency,addnoise)
            print(alpha," ",end='',flush=True)
    xlab='Mean number of photon (alpha^2)'
    ylab='H_min'
    title='Min Entropy evolution w/ Mean number of photon (efficiency = '+str(efficiency)+')'
    plotting(ninstart,nin,evol,xlab,ylab,title,limin,limax)

if(choice == 2): #grab a coffee, can be loooong
    ninstart = int(input("Min number of inputs: ") or 2)
    nin = int(input("Max number of inputs: ") or 3)
    effmin = float(input("Min efficiency: ") or 0.05)
    effmax = float(input("Max efficiency: ") or 0.99)
    effstep = float(input("Step effciency: ") or 0.02)
    efftmp = effmax/effstep
    evol=dict()
    almax=dict()
    for i in range(ninstart,nin+1):
        evol[i] = np.zeros(int(efftmp)+1)
        almax[i] = np.zeros(int(tmp)+1)
    x = np.arange(int(efftmp)+1)
    x = x*effstep
    for i in range(ninstart,nin+1):
        print("\nNb of inputs: ",i)
        startn = time.time()
        for j in range(int(effmin)+1,int(efftmp)+1):
            starteff = time.time()
            efficiency = effstep*j
            print("\n    Efficiency: ",efficiency)
            for k in range(int(limin)+1,int(tmp)+1):
                alpha = step*k
                almax[i][k]=entsolvDn(i,alpha,efficiency,addnoise)
                print(alpha," ",end='',flush=True)
            evol[i][j]=max(almax[i])
            stopeff = time.time()
            print("\n    Time elapsed for ",j,"eff: ", stopeff-starteff,"s" )
        stopn = time.time()
        print("\nTime elapsed for ",i," inputs: ",stopn-startn,"s")
    xlab='Efficiency'
    ylab='max(Hmin(alpha^2,efficiency))'
    title='Evolution of max(Hmin) for alpha^2 from '+str(limin)+' to '+str(limax)+' w/ efficiency'
    plotting(ninstart,nin,evol,xlab,ylab,title,effmin,effmax)
