# SDP-QRNG:

Implementation of the dual formulation of the SDP's used in this paper: https://arxiv.org/abs/1612.06566 , on Quantum Random Number Generator with a time based encoding

- Two states case: gqdual.py

- 2/3 states:
    - 2/3 states comparaison (Hmin(mean photon number)): SDP23.py
    - 2/3 states robusteness comparaison in efficiency modulation: EfficiencyMod.py

- Generalisation to n states: nstates.py w/ menu:
    - 1: plot Hmin(mean photon number) for N states
    - 2: plot Hmin(mean photon number) for N and M states
    - 3: plot max(H_min)=f(efficiency,mean photon number) for N to M states

<p align='center'><img src='https://gitlab.com/plut0n/SDP-QRNG/raw/master/Plot_example/menu.png'</p>
        
<p align='center'><img src='https://gitlab.com/plut0n/SDP-QRNG/raw/master/Plot_example/2345inputs.png'</p>
<p align='center'><img src='https://gitlab.com/plut0n/SDP-QRNG/raw/master/Plot_example/EffModFinal.png'</p>
