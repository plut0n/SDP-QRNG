import picos as pcs
import cvxopt as cvx
import os
from sympy.functions.special.tensor_functions import KroneckerDelta
import numpy as np
import mosek
import matplotlib.pyplot as plt

os.system('clear')
print("================== Upper bound, 2 and 3 inputs case (Dual formulation) ==================")

#3 input case
def entsolvD3(alpha,efficiency,phase,addnoise):
    olap = np.exp(-efficiency*alpha) #overlap(efficiency,mean photon number)
    olap_s = np.exp(-alpha)
    px = [1./3.]*3 #balanced input
    phi = phase
    cn = np.cos(phi)
    sn = np.sin(phi)
    phase= cn+sn*1j


    dim = 3 #3-D Hilbert Space
    i3 = cvx.matrix([1.,0.,0.,0.,1.,0.,0.,0.,1.],(3,3)) #identity matrix
    if (addnoise == True):
        err=0.001
        pbx = cvx.matrix([1-olap-2*err,err,err,olap,
            err,1-olap-2*err,err,olap,
            err,err,1-olap-2*err,olap],(4,3))
    else:
        pbx = cvx.matrix([1-olap,0,0,olap,
            0,1-olap,0,olap,
            0,0,1-olap,olap],(4,3))

    #Basis & state defintion
    b0 = phi0 = cvx.matrix([1.,0.,0.],(3,1))
    b1 = cvx.matrix([0.,1.,0.],(3,1))
    b2 = cvx.matrix([0.,0.,1.],(3,1))
    phi1 = olap_s*b0 + np.sqrt(1.-np.square(olap_s))*b1
    phi2 = olap_s*b0 + phase*olap_s*np.sqrt((1.-olap_s)/(1.+olap_s))*b1 + np.sqrt(1-np.square(olap_s)-np.square(olap_s)*((1.-olap_s)/(1.+olap_s)))*b2
    phi0 = cvx.matrix(phi0,tc='z')
    phi1 = cvx.matrix(phi1,tc='z')
    phi2 = cvx.matrix(phi2,tc='z')
    rho = [ phi0*phi0.H, phi1*phi1.H, phi2*phi2.H ]

    #SDP dual definition
    boundD=pcs.Problem()

    rho = pcs.new_param('rho',rho)
    pbx = pcs.new_param('pbx',pbx)
    i3 = pcs.new_param('i3',i3)
    px = pcs.new_param('px',px)

    nu = [ boundD.add_variable('nu{0}'.format(i),1) for i in range(12) ]
    H = [ boundD.add_variable('H[{0}]'.format(i),(3,3),vtype='hermitian') for i in range(8) ]

    for i in range(4): #over all the output 0,1,2,inconclusive
        boundD.add_list_of_constraints(
                [rho[0]*(px[0]*float(KroneckerDelta(i,3))*float(KroneckerDelta(3-max(j,3),0))+px[0]*float(KroneckerDelta(4-(min(j,4)),0))*(1-float(KroneckerDelta(i,3)))+nu[i])+rho[1]*(px[1]*float(KroneckerDelta(i,3))*float(KroneckerDelta(max(j%4,1)-1,0))+px[1]*(1-float(KroneckerDelta(i,3)))*float(KroneckerDelta(min(j%4,2)-1,1))+nu[i+4])+rho[2]*(px[2]*float(KroneckerDelta(i,3))*float(KroneckerDelta(j%2,0))+px[2]*(1-float(KroneckerDelta(i,3)))*float(KroneckerDelta(j%2,1))+nu[i+8])+H[j]-(1/3)*('I'|H[j])*i3 << 0 for j in range(8)],
                'j'
                )

    Objsum=0
    for i in range(12):
        Objsum += nu[i]*pbx[i]

    boundD.set_objective('min',-Objsum)
    #print(boundD)
    #boundD.solver_via_dual = True
    boundD.solve(solver='mosek',mosek_params={'presolve_tol_x' : 1.0e-15}, verbose=0)
    Objsum=0
    for i in range(12):
        Objsum += nu[i]*pbx[i]
    Entropy = - np.log2(-Objsum.value)
#    print(boundD.status)
#    print(alpha,Entropy)
    return Entropy

#2 input case
def entsolvD2(alpha,efficiency,addnoise):
    olap = np.exp(-efficiency*alpha) #Overlap(eta,alpha)
    olap_s = np.exp(-alpha)
    px = .5 #balanced input

    dim = 2 #2-D Hilbert Space
    i2 = cvx.matrix([1.,0.,0.,1.],(2,2)) #identity matrix
    if (addnoise == True):
        err=0.001
        pbx = cvx.matrix([1-olap-err,err,olap-err,err,1-olap-err,olap-err],(3,2)) #Probability of guessing b knowing x
    else:
        pbx = cvx.matrix([olap,1-olap,0,olap,0,1-olap],(3,2))


    #Basis & state defintion
    b0 = phi0 = cvx.matrix([1.,0.],(2,1))
    b1 = cvx.matrix([0.,1.],(2,1))
    phi1 = olap_s*b0 + np.sqrt(1-np.square(olap_s))*b1
    rho = [ phi0*phi0.T, phi1*phi1.T ]

    #SDP dual definition
    boundD=pcs.Problem()

    rho = pcs.new_param('rho',rho)
    pbx = pcs.new_param('pbx',pbx)
    i2 = pcs.new_param('i2',i2)

    nu = [ boundD.add_variable('nu{0}'.format(i),1) for i in range(6) ]
    H = [ boundD.add_variable('H[{0}]'.format(i),(2,2),vtype='hermitian') for i in range(4) ]

    for i in range(3):
        boundD.add_list_of_constraints(
                [rho[0]*(.5*float(KroneckerDelta(i,2))*(j%2)+.5*np.abs(j % 2-1)*(1-float(KroneckerDelta(i,2)))+nu[i])+rho[1]*(.5*float(KroneckerDelta(i,2))*float(KroneckerDelta(max(j-1,0),0))+.5*(1-float(KroneckerDelta(i,2)))*float(KroneckerDelta(min(j-2,0),0)) +nu[i+3])+H[j]-.5*('I'|H[j])*i2 << 0 for j in range(4)],
                'j'
                )

    Objsum=0
    for i in range(6):
        Objsum += nu[i]*pbx[i]

    boundD.set_objective('min',-Objsum)
#    print(boundD)
    #boundD.solver_via_dual = True
    boundD.solve(solver='mosek',mosek_params={'presolve_tol_x' : 1.0e-15}, verbose=0)
    Objsum=0
    for i in range(6):
        Objsum += nu[i]*pbx[i]
    Entropy = - np.log2(-Objsum.value)
#    print(boundD.status)
#    print(alpha,Entropy)
    return Entropy



#Main
print("Alpha (mean number of photon) limit :")
limin = input("Min alpha :") or 0.
limin = float(limin)
limax = input("Max alpha :") or 3.
limax = float(limax)
step = input("Step :") or .05
step = float(step)
efficiency = input("Efficiency :") or .77
efficiency = float(efficiency)
phase = input('Enter phase :') or 0.
phase = float(phase)
addnoise = input("Add noise : ") or True
tmp = limax/step
evol2D = np.zeros(int(tmp)+1)
evol3D0 = np.zeros(int(tmp)+1)
x = np.arange(int(tmp)+1)
x = x*step
for i in range(int(limin)+1,int(tmp)+1):
    alpha = step*i
    evol2D[i]=entsolvD2(alpha,efficiency,addnoise)
    evol3D0[i]=entsolvD3(alpha,efficiency,0.,addnoise)

ent2D = plt.plot(x,evol2D)
ent3D = plt.plot(x,evol3D0)
plt.xlabel('Mean number of photon')
plt.ylabel('H min')
plt.title('Evolution of min entropy with alpha^2 (two pulse  & eta = .77)')
plt.axis([limin,limax,min(evol3D0),max(evol3D0)+max(evol3D0)/10.])
plt.legend(['2 inputs','3 inputs (phase = 0)'],loc='upper right')
plt.show()
